;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  mean_dtheta.pro - This function calculates the mean of a function mapped
;                    onto the Legendre collocation points
;
;  usage:  result = mean_dtheta(A,costheta)
;
;                       1           1
;       where result =  -  integral     dx f(x)
;                       2          -1
;                     
;                       1           pi/2
;                    =  -  integral     d(theta) sin(theta) f(theta)
;                       2          -pi/2
;
;             A = input array/vector with theta dimension first
;             costheta = cosine of Legendre collocation points for theta grid
;
;  M.DeRosa - 20 Oct 2000 - created
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function mean_dtheta,A,costheta

;  preliminaries
naxin=size(A,/dim)
ndim=n_elements(naxin)
x=double(costheta)
nx=n_elements(x)

;  set output axes
if n_elements(naxin) eq 1 then naxout=1l else naxout=naxin(1:*)

;  error checking
if nx ne naxin(0) then begin
  print,'  mean_dtheta.pro:  size of costheta and first dim of A do not agree'
  return,-1
endif

;  reform input array
dim2=1l
if ndim gt 1 then for i=1,ndim-1 do dim2=dim2*naxin(i)
nax=[nx,dim2]
AA=transpose(reform(A,nax))

;  get integration weights
weights=weights_legendre(costheta)/(4*!dpi)

;  integrate
result=reform(weights##AA,naxout)

return,result
end
