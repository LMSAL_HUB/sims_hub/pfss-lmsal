;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  union.pro - This function returns the union of arr1 and arr2
;
;  usage:  result = union(arr1,arr2)
;
;  notes:  - Values are sorted and will only appear once in the union
;            even if they are repeated in either arr1 or arr2.
;
;  M.DeRosa - 1 Nov 1999 - created
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function union,arr1,arr2

;  usage_message
if n_params() eq 0 then begin
  print,'  indices = union(arr1,arr2)
  return,undef
endif

;  preliminaries
na1=n_elements(arr1)
na2=n_elements(arr2)

;  now determine union
case 1 of
  (na1 eq 0) and (na2 eq 0):  return,undef
  (na1 eq 0):  both=arr2(*)
  (na2 eq 0):  both=arr1(*)
  else:  both=[arr1(*),arr2(*)]
endcase
return,both(uniq(both,sort(both)))

end
