;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_draw_field2 - This procedure renders an image (or a series of images)
;                     of a magnetogram with field lines, but plots line
;                     crossings more accurately
;
;  usage:  pfss_draw_field2,bcent=bcent,lcent=lcent,
;            mag=mag,width=width,crop=crop,imsc=imsc,file=file,outim=outim,
;            /onscreen,/movie,/for_ps,/quiet,/nolines,/noimage
;          where (bcent,lcent) = central (lat,lon) in degrees of centroid of
;                                projection (default = (0,0))
;                mag = magnification of central image (default=1)
;                width = width of final image relative to central 
;                        magnetogram image (default=2.5)
;                crop = [x0,y0,x1,y1] cropping coordinates in normalized units
;                imsc = data value(s) to which to scale central magnetogram 
;                       image (default = image max, centered around 0)
;                thick = thickness of field lines
;                file = if set, FITS files of image(s) are created and no
;                       screen output is displayed, routine automatically
;                       adds .fits extension to filename and sets onscreen to
;                       false
;                outim = on output, image of z-buffer is read into this
;                        variable
;                onscreen = if set, then display image onscreen
;                movie = if set, creates movie sequence of field-line data,
;                        with each image rotated 1 degree from the last
;                for_ps = if set, then interchange white and black colors
;                nolines = if set, no field lines are drawn, supersedes both
;                          the drawopen and drawclosed keywords
;                noimage = if set, an opaque black sphere appears where the
;                          central image would have been (and because the
;                          sphere is opaque, field line segments behind it are
;                          not visible)
;                drawopen = if set, only open field lines are drawn
;                drawclosed = if set, only closed field lines are drawn
;                quiet = set to inhibit screen output
;
;           and in the common block we have
;                br = r-component of magnetic field
;                (ptr,ptth,ptph) = on input, contains a (n,stepmax)-array of
;                                  field line coordinates
;                nstep = an n-vector (where n=number of field lines) 
;                        containing the number of points comprising each 
;                        field line
;                rimage = on output, image of z=buffer is read into
;                         this variable
;
;  notes:  -crop keyword does not work with for_ps flag set
;          -if drawopen is set and drawclosed is not, would be faster to use
;           pfss_draw_field instead of this routine
;
;  M.DeRosa -  8 Feb 2002 - converted from an earlier script
;             29 May 2002 - added crop keyword
;             30 Jul 2002 - added for_ps,outim,onscreen keyword
;              1 Aug 2002 - added thick keyword
;              6 Aug 2002 - corrected for improper front-back projection of 
;                           field lines using "better Z-buffer", since IDL's 
;                           Z-buffer didn't take into account line crossings
;                           correctly (According to Neal, this "feature" is
;                           not limited to IDL's Z-buffer), but is MUCH slower
;             28 Oct 2002 - added quiet keyword
;              6 Nov 2002 - moved cropping to end of routine
;             19 Nov 2002 - removed open keyword, obsolete (info contained in
;                           kind and stbr arguments to trace_field)
;             11 Mar 2003 - added nolines keyword
;             25 Apr 2003 - added drawopen,drawclosed keywords
;             12 May 2003 - fixed logic with nolines,drawopen,drawclosed
;              4 Jun 2003 - for /onscreen, removed call to loadct_mld, and
;                           replaced with explicit color table indices
;             26 Aug 2003 - fixed bug related to lcent being a 1-element
;                           vector as opposed to a scalar
;             28 Jan 2004 - changed set_plot,'x' to SSW procedure set_x,
;                           which is Windows- and Mac-proof
;             15 Jun 2005 - changed set_x command to set_plot,olddname, where
;                           olddname is the device name contained in !d.name
;                           upon entry to this routine
;             12 Jul 2005 - added noimage keyword
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_draw_field2,bcent=bcent,lcent=lcent,$
  mag=mag,width=width,crop=crop,imsc=imsc,thick=thick,file=file,outim=outim,$
  onscreen=onscreen,movie=movie,for_ps=for_ps,nolines=nolines,noimage=noimage,$
  drawopen=drawopen,drawclosed=drawclosed,quiet=quiet

;  include common block
@pfss_data_block

;  some error checking
if n_elements(bcent) eq 0 then bcent=0.
if n_elements(lcent) eq 0 then lcent=0.
if n_elements(mag) eq 0 then mag=1
if n_elements(width) eq 0 then width=2.5
if n_elements(crop) eq 0 then crop=float([0,0,1,1])
if n_elements(movie) eq 0 then for_movie=0 else begin
  for_movie=1
  if n_elements(file) eq 0 then file='test'
endelse
if n_elements(file) ne 0 then begin
  onscreen=0
  if size(file,/type) ne 7 then file='test'
endif
if n_elements(thick) eq 0 then thick=1
if n_elements(imsc) eq 0 then imsc=max([-min(br(*,*,0)),max(br(*,*,0))])
if (n_elements(drawclosed) eq 0) and (n_elements(drawopen) eq 0) then begin
  drawclosed=1b
  drawopen=1b
endif
if n_elements(drawclosed) ne 0 $
  then drawclosed=drawclosed gt 0 else drawclosed=0b
if n_elements(drawopen) ne 0 $
  then drawopen=drawopen gt 0 else drawopen=0b

;  set colors, corresponds to ANA color table 47
gre=250b
blu=252b
whi=254b
bla=0b

;  preliminaries
cb=cos(bcent(0)*!dtor)
sb=-sin(bcent(0)*!dtor)
rmin=min(rix,max=rmax)
top=abs(ptr(0)-rmax) lt abs(ptr(0)-rmin)
if for_movie then nframe=360 else nframe=1
npt=n_elements(nstep)  ;  npt = number of field lines to be drawn

;  loop through images
if not keyword_set(quiet) then print,'  pfss_draw_field2: rendering image ...'
for j=0,nframe-1 do begin

  if for_movie and (not keyword_set(quiet)) then $
    pfss_print_time,'  ',j+1,nframe,tst,slen
  if for_movie then lcent=j

  ;  get image of full-disk magnetogram, use z-buffer
  olddname=!d.name  &  set_plot,'z'
  if keyword_set(for_ps) then erase,whi else erase,bla
  if n_elements(bounds) gt 0 then begin
    mgram=[br(*,*,0),br(*,*,0),br(*,*,0)]
    if keyword_set(noimage) then $
      mgram=make_array(dim=size(mgram,/dim),value=min([imsc(0),-imsc(0)]))
    olon=[lon-!radeg*(bounds(3)-bounds(2)),lon,$
      lon+!radeg*(bounds(3)-bounds(2))]
    scim,mgram,m=mag,ortho=[lcent(0) mod 360,bcent(0)],olon=olon,olat=lat,$
      sc=imsc,/quiet,top=248,white=keyword_set(for_ps)
  endif else begin
    mgram=br(*,*,0)
    if keyword_set(noimage) then $
      mgram=make_array(dim=size(mgram,/dim),value=min([imsc(0),-imsc(0)]))
    scim,mgram,m=mag,ortho=[lcent(0) mod 360,bcent(0)],olon=lon,olat=lat,$
      sc=imsc,/quiet,top=248,white=keyword_set(for_ps)
  endelse
  outim=tvrd()

  ;  open z-buffer and display magnetogram
  nax=size(outim,/dim)
  winxsz=round(nax(0)*width)
  winysz=round(nax(1)*width)
  xcent=round(nax(0)*width*0.5)
  zcent=round(nax(1)*width*0.5)
  rad=0.5*nax(0)
  pos=[xcent,zcent,xcent,zcent]+[-1,-1,1,1]*round(rad)
  device,set_resolution=[winxsz,winysz]
  t3d,/reset
  if keyword_set(for_ps) then erase,whi else erase,bla
  tv,outim,pos(0),pos(1),z=0,/t3d
  outim=tvrd()

  ;  create depth map array
  xgrid=lindgen(winxsz)#replicate(1l,winysz)
  zgrid=replicate(1l,winxsz)#lindgen(winysz)
  r2grid=((xgrid-xcent)^2+(zgrid-zcent)^2)/(rad^2)
  wh=where(r2grid le 1)
  dmap=make_array(dim=[winxsz,winysz],value=-width)
  dmap(wh)=0.0

  ;  establish grid for plotting field lines
  plot,[-1,1],[-1,1],pos=pos,/noerase,/device,/nodata,xsty=4,ysty=4

  ;  now draw the individual field lines
  if not keyword_set(nolines) then begin
    open=intarr(npt)
    for i=0,npt-1 do begin

      ;  print update message
      if ((not keyword_set(quiet)) and (not for_movie)) then $
        pfss_print_time,'  pfss_draw_field2: ',i+1,npt,tst,slen

      ;  transform from spherical to cartesian coordinates
      ns=nstep(i)
      xp=ptr(0:ns-1,i)*sin(ptth(0:ns-1,i))*sin(ptph(0:ns-1,i)-lcent(0)*!dtor)
      yp=ptr(0:ns-1,i)*sin(ptth(0:ns-1,i))*cos(ptph(0:ns-1,i)-lcent(0)*!dtor)
      zp=ptr(0:ns-1,i)*cos(ptth(0:ns-1,i))

      ;  now latitudinal tilt
      xpp=xp
      ypp=cb*yp-sb*zp
      zpp=sb*yp+cb*zp

      ;  determine whether line is open or closed 
      if (max(ptr(0:ns-1,i))-rmin)/(rmax-rmin) gt 0.99 then begin
        irc=get_interpolation_index(rix,ptr(0,i))
        ithc=get_interpolation_index(lat,90-ptth(0,i)*!radeg)
        iphc=get_interpolation_index(lon,(ptph(0,i)*!radeg+360) mod 360)
        brc=interpolate(br,iphc,ithc,irc)
        if brc gt 0 then open(i)=1 else open(i)=-1
      endif  ;  else open(i)=0, which has already been done

      ;  only plot those lines that are higher than the first radial gridpoint
      heightflag=max(ptr(0:ns-1,i)) gt rix(1)
      drawflag=(drawopen and (open(i) ne 0)) or (drawclosed and (open(i) eq 0))
      if (heightflag and drawflag) then begin

        ;  hide line segments that are behind disk
        wh1=where(ypp ge 0,nwh1)
        wh2=where((ypp lt 0) and ((xpp^2+zpp^2) gt rix(0)^2),nwh2)
        case 1 of
          (nwh1 gt 0) and (nwh2 gt 0): wh=union(wh1,wh2)
          (nwh1 gt 0) and (nwh2 eq 0): wh=wh1
          (nwh1 eq 0) and (nwh2 gt 0): wh=wh2
          (nwh1 eq 0) and (nwh2 eq 0): doline=0
        endcase
        if (nwh1+nwh2) gt 0 then doline=1

        if doline then begin
  
          ;  select the visible coordinates of the line
          xpp=xpp(wh)
          ypp=ypp(wh)
          zpp=zpp(wh)
 
          ;  determine color
          case open(i) of
            -1: col=blu
             0: if keyword_set(for_ps) then col=bla else col=whi
             1: col=gre
          endcase
 
          ;  plot lines in z-buffer and read out
          if keyword_set(for_ps) then erase,whi else erase,bla
          plots,xpp,zpp,ypp,col=col,/t3d,thick=thick
          buffer=tvrd()

          ;  determine depth of each pixel in the line
          wh=where(buffer eq col,nwh)
          if nwh gt 0 then begin
            dline=fltarr(nwh)  ;  depth of each pixel comprising line
            whx=wh mod winxsz  &  whz=wh/winysz
            xdist=sqrt(((whx-xcent)/rad)^2)  ;  x-distance from center
            zdist=sqrt(((whz-zcent)/rad)^2)  ;  z-distance from center
            for k=0l,nwh-1 do begin
              dist=sqrt((xpp-xdist(k))^2+(zpp-zdist(k))^2)
              mdist=min(dist,dix)  ;  dix=point on line closest to target pixel
              dline(k)=ypp(dix)
            endfor

            ;  compare depth of each point in line to depth in depth map
            repl=where(dline gt dmap(wh),nrepl)
            if nrepl gt 0 then begin
              dmap(wh(repl))=dline(repl)
              outim(wh(repl))=col
            endif
          endif

        endif
      endif
    endfor

  endif

  ;  now crop image if desired
  outim=outim(round(crop(0)*winxsz)>0:round(crop(2)*winxsz)<(winxsz-1),$
    round(crop(1)*winysz)>0:round(crop(3)*winysz)<(winysz-1))

  ;  capture image
  set_plot,olddname  ;  exit z-buffer (used to be set_plot,'x' and set_x)
  if keyword_set(onscreen) then begin
    re=[bindgen(250),0b,0b,255b,255b,255b,255b]
    gr=[bindgen(250),255b,255b,0b,0b,255b,255b]
    bl=[bindgen(250),0b,0b,255b,255b,255b,255b]
    tvlct,re,gr,bl
    scim,outim
  endif else begin
    if for_movie then begin
      writefits,file+get_string_number(j,pad=3)+'.fits',outim
    endif else if keyword_set(file) then writefits,file+'.fits',outim
  endelse

endfor

rimage=outim

end
