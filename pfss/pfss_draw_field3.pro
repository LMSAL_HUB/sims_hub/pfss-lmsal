;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_draw_field3 - This procedure renders an image (or a series of images)
;                     of a magnetogram with field lines, but uses object
;                     graphics to plot line crossings more accurately
;
;  usage:  pfss_draw_field3,bcent=bcent,lcent=lcent,
;            mag=mag,width=width,crop=crop,imsc=imsc,file=file,outim=outim,
;            /onscreen,/movie,/for_ps,/quiet,/nolines,/noimage
;          where (bcent,lcent) = central (lat,lon) in degrees of centroid of
;                                projection (default = (0,0))
;                mag = magnification of central image (default=1)
;                width = width of final image relative to central 
;                        magnetogram image (default=2.5)
;                crop = [x0,y0,x1,y1] cropping coordinates in normalized units
;                imsc = data value(s) to which to scale central magnetogram 
;                       image (default = image max, centered around 0)
;                thick = thickness of field lines
;                file = if set, FITS files of image(s) are created and no
;                       screen output is displayed, routine automatically
;                       adds .fits extension to filename and sets onscreen to
;                       false
;                outim = on output, final image is read into this variable
;                onscreen = if set, then display image onscreen
;                movie = if set, creates movie sequence of field-line data,
;                        with each image rotated 1 degree from the last
;                for_ps = if set, then interchange white and black colors
;                nolines = if set, no field lines are drawn, supersedes both
;                          the drawopen and drawclosed keywords
;                noimage = if set, an opaque black sphere appears where the
;                          central image would have been (and because the
;                          sphere is opaque, field line segments behind it are
;                          not visible)
;                drawopen = if set, only open field lines are drawn
;                drawclosed = if set, only closed field lines are drawn
;                quiet = set to inhibit screen output
;
;           and in the common block we have
;                br = r-component of magnetic field
;                (ptr,ptth,ptph) = on input, contains a (n,stepmax)-array of
;                                  field line coordinates
;                nstep = an n-vector (where n=number of field lines) 
;                        containing the number of points comprising each 
;                        field line
;                rimage = on output, final image is read into this variable
;
;  notes: -Unlike pfss_draw_field and pfss_draw_field2, this routine produces
;          a 24-bit (true-color) image.  To convert to an indexed image, one
;          can use IDL's color_quan function as follows:
;            imindexed=color_quan(outim,1,red,green,blue)
;         -Occasionally, the draw method from the IDLgrBuffer destination
;          object sometimes generates underflow, overflow, and illegal
;          operand errors (at least in IDL v.6.1), but these tend to happen
;          only intermittently, and I don't see any problems with the output,
;          so I usually ignore the errors.  I believe these are exceptions
;          returned from the renderer, and IDL passes them on.
;
;  M.DeRosa - 22 Aug 2006 - created
;             12 Nov 2010 - due to the way IDL version 8 interprets the "dot
;                           notation" wrt objects, I had to use square
;                           brackets instead of parentheses to get lines such
;                           as "olist.ofieldlines[i]->getproperty,color=col"
;                           to compile
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_draw_field3,bcent=bcent,lcent=lcent,$
  mag=mag,width=width,crop=crop,imsc=imsc,thick=thick,file=file,outim=outim,$
  onscreen=onscreen,movie=movie,for_ps=for_ps,nolines=nolines,noimage=noimage,$
  drawopen=drawopen,drawclosed=drawclosed,quiet=quiet

;  include common block
@pfss_data_block

;  some error checking
if n_elements(bcent) eq 0 then bcent=0.
if n_elements(lcent) eq 0 then lcent=0.
if n_elements(mag) eq 0 then mag=1
if n_elements(width) eq 0 then width=2.5
if n_elements(crop) eq 0 then crop=float([0,0,1,1])
if n_elements(movie) eq 0 then for_movie=0 else begin
  for_movie=1
  if n_elements(file) eq 0 then file='test'
endelse
if n_elements(file) ne 0 then begin
  onscreen=0
  if size(file,/type) ne 7 then file='test'
endif
if n_elements(thick) eq 0 then thick=1
if n_elements(imsc) eq 0 then imsc=max(abs(minmax(br(*,*,0))))
if (n_elements(drawclosed) eq 0) and (n_elements(drawopen) eq 0) then begin
  drawclosed=1b
  drawopen=1b
endif
if n_elements(drawclosed) ne 0 $
  then drawclosed=drawclosed gt 0 else drawclosed=0b
if n_elements(drawopen) ne 0 $
  then drawopen=drawopen gt 0 else drawopen=0b

;  some RGB colors
bla=byte([  0,  0,  0])
gre=byte([  0,255,  0])
red=byte([255,  0,255])
whi=byte([255,255,255])

;  create view object
pfss_view_create,olist

;  create and add a texture map to the view
if n_elements(im_data) eq 0 then $
  im_data=spherical_image_create((br)(*,*,0),lon,lat)
osphere2=spherical_texmap_create(im_data,imsc=imsc)
olist.omodelin->add,osphere2

;  preliminaries
nlines=n_elements(olist.ofieldlines)
if for_movie then nframe=360 else nframe=1
rmax=max(rix)

;  correct background color and correct colors for fieldlines
if keyword_set(for_ps) then begin
  olist.oview->setproperty,color=whi
  for i=0,nlines-1 do begin
    olist.ofieldlines[i]->getproperty,color=col
    if total(col eq whi,/pres) eq 3b then $
      olist.ofieldlines[i]->setproperty,color=bla
  endfor
endif

;  remove central image if desired
if keyword_set(noimage) then obj_destroy,osphere2

;  set line thicknesses
for i=0,nlines-1 do olist.ofieldlines[i]->setproperty,thick=thick

;  remove lines if desired
if keyword_set(nolines) then begin
  obj_destroy,olist.ofieldlines
endif else begin
  doline=bytarr(nlines)
  for i=0,nlines-1 do begin
    olist.ofieldlines[i]->getproperty,color=col
    if (total(col eq gre,/pres) eq 3b) and (drawopen) then doline(i)=1b
    if (total(col eq red,/pres) eq 3b) and (drawopen) then doline(i)=1b
    if (total(col eq bla,/pres) eq 3b) and (drawclosed) then doline(i)=1b
    if (total(col eq whi,/pres) eq 3b) and (drawclosed) then doline(i)=1b
  endfor
  wh=where(doline eq 0b,nwh)
  if nwh gt 0 then obj_destroy,olist.ofieldlines(wh)
endelse

;  loop through images
if not keyword_set(quiet) then print,'  pfss_draw_field3: rendering image ...'
for j=0,nframe-1 do begin

  if for_movie and (not keyword_set(quiet)) then $
    pfss_print_time,'  ',j+1,nframe,tst,slen
  if for_movie then lcent=j

  ;  get correct orientation
  olist.omodelin->reset
  olist.omodelin->rotate,[1,0,0],-90
  olist.omodelin->rotate,[0,1,0],-90-lcent
  olist.omodelin->rotate,[1,0,0],bcent

  ;  capture image
  owindow=obj_new('IDLgrBuffer',graph=olist.oview, $
    dim=[nlat,nlat]*mag*rmax)
  owindow->draw
  owindow->getproperty,image_data=outim

  ;  destroy object hierarchy
  obj_destroy,owindow

  ;  now reduce to proper width
  naxoutim=size(outim,/dim)
  winxsz=naxoutim(1)
  winysz=naxoutim(2)
  wfrac=width/max(rmax)
  wcrp=[(1-wfrac)/2,(1-wfrac)/2,1-(1-wfrac)/2,1-(1-wfrac)/2]
  outim=outim(*,round(wcrp(0)*winxsz)>0:(round(wcrp(2)*winxsz)-1)<(winxsz-1),$
    round(wcrp(1)*winysz)>0:(round(wcrp(3)*winysz)-1)<(winysz-1))

  ;  now crop image if desired
  naxoutim=size(outim,/dim)
  winxsz=naxoutim(1)
  winysz=naxoutim(2)
  outim=outim(*,round(crop(0)*winxsz)>0:round(crop(2)*winxsz)<(winxsz-1),$
    round(crop(1)*winysz)>0:round(crop(3)*winysz)<(winysz-1))

  ;  capture image
  if keyword_set(onscreen) then begin
    scim,outim,/quiet,/true
  endif else begin
    if for_movie then begin
      writefits,file+get_string_number(j,pad=3)+'.fits',outim
    endif else if keyword_set(file) then writefits,file+'.fits',outim
  endelse

endfor

;  rimage is a 24-bit color image
rimage=outim

;  clean up
obj_destroy,owindow

end
