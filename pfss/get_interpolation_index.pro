;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  get_interpolation_index - This function returns the fractional coordinate 
;                            index of an input array
;
;  usage:  result=get_interpolation_index(array,value)
;            where array = input array
;                  value = values for which index is desired
;
;  notes:  -useful for determining the index for interpolation,  
;           get_interpolation_index(linrange(11,0,2),.68) returns 3.4, since 
;           the value .68 would lie at (fractional) index coordinate 3.4
;          -if out of bounds, then function returns either 0 or N-1, where N
;           is the number of elements in array
;          -assumes that array is monotonically increasing
;          -linear interpolation performed, watch out if array is not equally 
;           spaced
;
;  M.DeRosa - 12 Oct 2001 - created
;             12 Aug 2002 - value argument now takes arrays as well as scalars
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function get_interpolation_index,array,value

npt=n_elements(value)
out=dblarr(npt,/noz)
for i=0l,npt-1 do begin
  if value(i) le array(0) then begin
    out(i)=0d
  endif else begin
    nearix=where(array gt value(i),nwh)
    if nwh gt 0 then begin
      nix=nearix(0)-1
      extract=array(nix:nix+1)
      out(i)=nix+(value(i)-extract(0))/double(extract(1)-extract(0))
    endif else out(i)=double(n_elements(array)-1)
  endelse
endfor

return,out

end