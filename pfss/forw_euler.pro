;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  forw_euler.pro - This function integratess a system of equations of the
;                   form dy1/dx = f1(x,y1,y2,...) forward in x space
;                        dy2/dx = f2(x,y1,y2,...)
;                          etc.
;
;  usage:  result = forw_euler(x,y,h,derivs)
;          where result = a vector of values of y(x+h)
;                x = the current value of the independent variable
;                y = a vector of values of y(x)
;                h = amount to step forward in x
;                derivs = either: a string specifying a user-supplied idl 
;                                 function that calculates the derivatives 
;                                 dy/dx evaluated at an arbitrary point x,
;                         or: a vector of values of dy/dx evaluated at x
;
;  M.DeRosa - 18 Oct 2001 - created
;             28 Jun 2012 - added y argument to call of user-supplied function
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function forw_euler,x,y,h,derivs

;  usage message
if n_params() ne 4 then begin
  print,' result = forw_euler(x,y,h,derivs)'
  return,-1
endif 

;  preliminaries
order=n_elements(y)
dtyp=size(derivs,/type)
if dtyp eq 7 then begin
  callf=1
endif else begin
  callf=0
  if n_elements(derivs) lt order then begin
    print,'  forw_euler:  derivs argument has fewer elements than y'
    return,-1
  endif
endelse

;  now advance solution and return
if callf then dydx=call_function(derivs(0),x(0),y) else dydx=derivs(0:order-1)
return,y+h(0)*dydx

end
