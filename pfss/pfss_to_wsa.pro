;+
; NAME: pfss_to_wsa
;
; PURPOSE:
;   Given a LMSAL assimilation model snapshot, such as the structure returned
;   from pfss_surffield_restore, this routine will write out a FITS file in a
;   format that can be read by the WSA model.
;
; CALLING SEQUENCE:
;   pfss_to_wsa,sfield,outdir=outdir
;
; INPUTS:
;   sfield = output from pfss_surffield_restore.pro, or another compatible
;            structure, having the tags FLUXS, PHIS, THETAS, NFLUX, I,
;            RUNNUMBER, and NOW, which contain arrays of the locations and
;            strengths of surface-flux concentrations and ancillary data.
;
; KEYWORD PARAMETERS:
;   outdir = directory in which to save file (default is current directory)
;   xsiz = number of gridpoints in longitude (deafult=360)
;   ysiz = number of gridpoints in latitude (deafult=180)
;
; OUTPUTS:
;   One FITS file that can be read in by the WSA pipeline
;
; NOTES: -The output FITS files is based on file spec from Shaela Jones 
;        -The output filename is based on date of model.
;        -Essentially, the routine takes the list of flux elements and assigns
;         them to pixels in a synoptic map.
;        -Because the LMSAL assimilation model maps are snapshots at a   
;         point in time, so:
;         CRROTEDG = Carr rot number of central meridian, as viewed from Earth
;         MAPCR = Carr rot number of central meridian, as viewed from Earth
;         MODELDA = not sure what to put here (-1 is used as a dummy value)
;         MODELVER = not sure what to put here (-1 is used as a dummy value)
;
; EXAMPLE:
;   date='2014-04-08'  ;  sample date
;   fname=pfss_time2file(date,/ssw,/url,/surffield)
;   sfield=pfss_surffield_restore(fname[0])
;   pfss_to_wsa,sfield
; 
; MODIFICATION HISTORY:
;   M.DeRosa - 5 Apr 2021 - created
;
;-

pro pfss_to_wsa,sfield,outdir=outdir,xsiz=xsiz,ysiz=ysiz

;  parameters
if ~keyword_set(xsiz) then xsiz=360  ;  number of elements in longitude in map
if ~keyword_set(ysiz) then ysiz=180  ;  number of elements in latitude in map
crval1=180.0         ;  coordinate of reference pixel in x
crval2=0.0           ;  coordinate of reference pixel in y
cunit1='deg     '    ;  unit of x coordinate
cunit2='deg     '    ;  unit of y coordinate
ctype1='CRLN-CAR'    ;  axis label for x
ctype2='CRLT-CAR'    ;  axis label for y

;  define map propjection and WCS keywords
crpix1=0.5*xsiz+0.5  ;  reference pixel in x
crpix2=0.5*ysiz+0.5  ;  reference pixel in y
cdelt1=360./xsiz     ;  delta x
cdelt2=180./ysiz     ;  delta y

;  next create carrington map based on above WCS projection
synoptic=fltarr(xsiz,ysiz)
xix=(fix((sfield.phis*180/!dpi)/cdelt1)) mod xsiz  ;  index of x pixel
yix=(fix((180-sfield.thetas*180/!dpi)/cdelt2)) mod ysiz  ;  index of y pixel
for ii=0l,sfield.nflux-1 do synoptic(xix(ii),yix(ii))+=(sfield.fluxs(ii))

;  create FITS header
mkhdr,hd,synoptic
sxaddpar,hd,'CRPIX1',crpix1
sxaddpar,hd,'CRPIX2',crpix2
sxaddpar,hd,'CRVAL1',crval1,' center of map: Carrington long = 180 deg'
sxaddpar,hd,'CRVAL2',crval2,' center of map: heliographic lat = equator'
sxaddpar,hd,'CDELT1',cdelt1
sxaddpar,hd,'CDELT2',cdelt2
sxaddpar,hd,'CUNIT1',cunit1
sxaddpar,hd,'CUNIT2',cunit2
sxaddpar,hd,'CTYPE1',ctype1
sxaddpar,hd,'CTYPE2',ctype2
sxaddpar,hd,'CRROTEDG',(fix(tim2carr(sfield.now,/dc)))[0]
sxaddpar,hd,'CRLNGEDG',0.0
sxaddpar,hd,'LATTYPE',0  ;  assume 0=latitude and 1=colatitude
sxaddpar,hd,'MAPDATA','HMI     '
sxaddpar,hd,'GRID',cdelt1  ;  assume cdelt1=cdelt2
sxaddpar,hd,'MAPCR',(tim2carr(sfield.now,/dc))[0]
exd=anytim(sfield.now,/ex)
sxaddpar,hd,'MAPJUL',julday(exd[5],exd[4],exd[6],exd[0],exd[1],exd[2])
sxaddpar,hd,'MAPTIME',anytim(sfield.now,/ccsds)
sxaddpar,hd,'MODEL','LMSALsft',' LMSAL surface flux transport model'
sxaddpar,hd,'MODELDA',-1
sxaddpar,hd,'MODELVER',-1.0

;  write out FITS file
outfile='LMSALassim_'+strjoin(strsplit(strjoin(strsplit(sfield.now,' ',/ex),'_'),':/',/ex))+'.fits'
if keyword_set(outdir) then outfile=outdir + outfile
writefits,outfile,synoptic,hd
print,'  pfss_to_wsa: wrote ' + outfile

end
