;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  pfss_potl_field.pro - This procedure computes the potential magnetic field
;                        B(r,theta,phi) given the potential Phi(l,m,r)
;
;  usage: pfss_potl_field,rtop,rgrid,rindex=rindex,thindex=thindex,
;           phindex=phindex,lmax=lmax,/trunc,potl=potl,/quiet
;         where rtop=radius of uppermost gridpoint
;               rgrid=sets radial gridpoint spacing:
;                      1 = equally spaced (default)
;                      2 = grid spacing varies with r^2
;                      3 = custom radial grid given by the rindex keyword 
;               rindex = custom array of radial coordinates for output grid
;               thindex = (optional) custom array of theta (colatitude)
;                         coordinates, in radians, for output grid.  If not
;                         specified existing latitudinal grid is used.
;               phindex = (optional) custom array of phi (longitude)
;                         coordinates, in radians, for output grid.  If not
;                         specified, existing longitudinal grid is used.
;               lmax=if set, only use this number of spherical harmonics in
;                    constructing the potential (and thus the field) 
;               trunc=set to use fewer spherical harmonics when
;                     reconstructing B as you get farther out in radius
;               potl=contains potl if desired, but what you pass
;                    to this routine must not be undefined in order
;                    for the field potential to be computed
;               quiet = set for minimal screen output
;
;         and in the common block we have:
;               phiat=on input, (l,m) array of dcomplex coeffs, 
;                     corresponding to r^l eigenfunction
;               phibt=on input, (l,m) array of dcomplex coeffs, 
;                     corresponding to 1/r^(l+1) eigenfunction
;               (br,bth,bph)=in output, (r,theta,phi) components of B-field
;
;  Notes: -If thindex and phindex are set, the variables
;          nlon,nlat,lon,lat,theta,phi in the common block are reset to the
;          values commensurate with the arrays given in thindex and phindex.
;          The old values (which correspond to the magnetic field used in the
;          potential extrapolation) are lost forever!
;         -Be careful when using thindex to put points at theta=0 and/or at
;          theta=!dpi.  Usually this routine returns Inf's at these points in
;          Bth and Bph, which might cause problems down the road.
;
;  M.DeRosa - 30 Jan 2002 - converted from earlier script
;              8 Feb 2002 - added lmax keyword
;              2 Jul 2002 - added quiet keyword
;             30 Apr 2003 - now utilizes memory more efficiently, based on a
;                           suggestion from Bart De Pontieu
;             12 May 2003 - converted common block to PFSS package format
;             27 Mar 2007 - added rgrid option 3 and rindex,thindex,phindex
;                           keywords
;             23 May 2007 - fixed bug with nlat when thindex is specified
;             30 Nov 2007 - fixed bug with truncated l arrays
;             15 Apr 2009 - added check to see if rgrid is provided
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro pfss_potl_field,rtop,rgrid,rindex=rindex,thindex=thindex,phindex=phindex,$
  lmax=lmax,trunc=trunc,potl=potl,quiet=quiet

;  print usage message
if n_params() eq 0 then begin
  print,'  pfss_potl_field,rtop,rgrid,rindex=rindex,thindex=thindex,'+$
    'phindex=phindex,lmax=lmax,/trunc,potl=potl,/quiet'
  return
endif

;  include common block
@pfss_data_block

;  preliminaries
nlat0=nlat
if keyword_set(lmax) then lmax=lmax<nlat0 else lmax=nlat0
cth=cos(theta)

;  get l and m index arrays of transform
phisiz=size(phiat,/dim)
lix=lindgen(phisiz(0))
mix=lindgen(phisiz(1))
larr=lix#replicate(1,phisiz(1))
marr=replicate(1,phisiz(0))#mix
wh=where(marr gt larr)
larr(wh)=0  &  marr(wh)=0

;  get radial grid
dr0=(!dpi/nlat0)  ;  r grid spacing at r=1, make it half avg lat grid spacing
rra=[1d0,double(rtop(0))]  ;  range of r
if n_elements(rgrid) eq 0 then rgrid=1
case rgrid of
  2: begin  ;  radial gridpoint separation is proportional to r^2
    rix=[rra(0)]
    lastr=rra(0)
    repeat begin 
      nextr=lastr+dr0*(lastr/rra(0))^2
      rix=[rix,nextr]
      lastr=nextr
    endrep until nextr ge rra(1)
    rix2=rix/((max(rix)-rra(0))/(rra(1)-rra(0)))
    rix=rix2+(rra(0)-rix2(0))
    nr=n_elements(rix)
    end
  3: begin  ;  custom radial grid
    if n_elements(rindex) eq 0 then begin
      print,'  ERROR in pfss_potl_field: rindex must be set if rgrid=3'
      return
    endif
    rix=rindex
    nr=n_elements(rindex)
    end
  else: begin  ;  radial gridpoints uniformly spaced
    nr=round((rra(1)-rra(0))/dr0)
    rix=linrange(nr,rra(0),rra(1))
    end
endcase
if not keyword_set(quiet) then $
  print,'  pfss_potl_field: nr = '+strcompress(nr,/r)

;  set up theta grids
if n_elements(thindex) gt 0 then begin
  if max(thindex,min=minthindex) gt !dpi then begin
    print,'  ERROR in pfss_potl_field: thindex out of range'
    return
  endif else if minthindex lt 0d0 then begin
    print,'  ERROR in pfss_potl_field: thindex out of range'
    return
  endif 
  ntheta=n_elements(thindex)
  theta=thindex
  nlat=ntheta
  lat=90-theta*180/!dpi
endif else begin
  ntheta=nlat
  thindex=theta
endelse

;  set up phi grid
if n_elements(phindex) gt 0 then begin
  nphi=n_elements(phindex)
  phi=phindex
  nlon=nphi
  lon=phi*180/!dpi
endif else begin
  nphi=nlon
  phindex=phi
endelse

;  set up planar sin(theta) array
stharr=replicate(1,nphi)#sqrt(1-(cos(thindex))^2)

;  compute lmax for each radius
lmaxarr=lonarr(nr,/noz)
if keyword_set(trunc) then begin  ;  include fewer l modes as you get higher up
  lmaxarr(0)=nlat0<lmax
  for i=1,nr-1 do begin
    wh=where(rix(i)^lindgen(nlat0+1) gt 1e6,nwh)
    if nwh eq 0 then lmaxarr(i)=lmax else lmaxarr(i)=(wh(0)<lmax)
  endfor
endif else lmaxarr(*)=nlat0<lmax  ;  otherwise do nlat transforms for all radii

;  compute Br in (r,l,m)-space
bt=make_array(dim=[phisiz,nr],/noz,/dcomplex)
for i=0,nr-1 do $
  bt(*,*,i)= phiat*larr*rix(i)^(larr-1) - phibt*(larr+1)*rix(i)^(-larr-2)

;  ...and then transform to (r,theta,phi)-space
br=make_array(dim=[nphi,ntheta,nr],/float,/noz)
for i=0,nr-1 do begin
  if not keyword_set(quiet) then $
    pfss_print_time,'  pfss_potl_field: computing Br:  ',i+1,nr,tst,slen,/elap
  br(*,*,i)=inv_spherical_transform(bt(*,*,i),cth,lmax=lmaxarr(i),$
    thindex=thindex,phindex=phindex)
endfor

;  compute sin(theta) * Bth in (r,l,m)-space...
factor=sqrt(double(larr^2-marr^2)/double(4*larr^2-1))
for i=0,nr-1 do begin
  bt(*,*,i)=(larr-1)*factor* $
    (shift(phiat,1,0)*rix(i)^(larr-2) + shift(phibt,1,0)*rix(i)^(-larr-1)) $
    - (larr+2)*shift(factor,-1,0)* $
    (shift(phiat,-1,0)*rix(i)^larr + shift(phibt,-1,0)*rix(i)^(-larr-3))
  bt(0,0,i)=-2*factor(1,0)*(phiat(1,0) + phibt(1,0)*rix(i)^(-3))
  bt(lmax,*,i)=(lmax-1)*factor(lmax,*)* $
    (phiat(lmax-1,*)*rix(i)^(lmax-2) + phibt(lmax-1,*)*rix(i)^(-lmax-1))
endfor

;  ...and then compute Bth in (r,theta,phi)-space
bth=make_array(dim=[nphi,ntheta,nr],/float,/noz)
for i=0,nr-1 do begin
  if not keyword_set(quiet) then $
    pfss_print_time,'  pfss_potl_field: computing Bth:  ',i+1,nr,tst,slen,/elap
  bth(*,*,i)=inv_spherical_transform(bt(*,*,i),cth,lmax=lmaxarr(i),$
    thindex=thindex,phindex=phindex)/stharr
endfor

;  compute sin(theta) * Bph in (r,l,m)-space...
for i=0,nr-1 do bt(*,*,i)=complex(0,1)*marr* $
  (phiat*rix(i)^(larr-1) + phibt*rix(i)^(-larr-2))

;  ...and then compute Bph in (r,theta,phi)-space
bph=make_array(dim=[nphi,ntheta,nr],/float,/noz)
for i=0,nr-1 do begin
  if not keyword_set(quiet) then $
    pfss_print_time,'  pfss_potl_field: computing Bph:  ',i+1,nr,tst,slen,/elap
  bph(*,*,i)=inv_spherical_transform(bt(*,*,i),cth,lmax=lmaxarr(i),$
    thindex=thindex,phindex=phindex)/stharr
endfor

;  now transform the field potential to (r,theta,phi)-space
if n_elements(potl) gt 0 then begin
  potl=make_array(dim=[nlon,nlat,nr],/float,/noz)
  for i=0,nr-1 do begin
    if not keyword_set(quiet) then $
      pfss_print_time,'  pfss_potl_field: computing the field potential:  ',$
        i+1,nr,tst,slen,/elap
      potl(*,*,i)=inv_spherical_transform(phibt*rix(i)^(-larr-1)+ $
        phiat*rix(i)^larr,cth,lmax=lmax)
  endfor
endif

end      
