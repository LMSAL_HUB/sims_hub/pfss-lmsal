;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  logrange.pro - creates a double precision array of n numbers starting
;                 with min and ending with max, in a logarithmic fashion.
;
;  usage:  result=logrange(n,min,max)
;          where result = double-precision array with n elements
;                     n = number of points in the array
;                   max = highest number
;                   min = lowest number
;
;  notes:  returns -1 if error detected
;
;  M.DeRosa - 12 Apr 1995 - created
;             30 Nov 1998 - added usgae message
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

function logrange,n,min,max

;  usage message
if n_params() lt 3 then begin
  print,'  result=logrange(n,min,max)'
  return,-1
endif

np=long(n)
lmin=alog10(double(min))
lmax=alog10(double(max))

vec=dindgen(np)
vec=10^linrange(np,lmin,lmax)

return,vec
end

