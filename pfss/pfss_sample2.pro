;  This sample script demonstrates how to do an extrapolation with a
;  source surface radius of 2.0 R_sun (as opposed to the [canonical] radius
;  of 2.5 that is used for the pre-computed field models available through
;  this SSW package).

;  To use, do a  .r pfss_sample2  (this file) at the IDL> prompt.

;  M.DeRosa -  4 Aug 2010 - created

;  include common block (not necessary but useful for looking at things...)
@pfss_data_block

;  first download a surface-field magnetic field map 

;  date/time is set here to Apr 5, 2003 for demonstration purposes, but any
;  SSW formatted date/time will do
sfield=pfss_surffield_restore(pfss_time2file('2003-04-05',/ssw_cat,/url,/surffield))  ;  for all users
;sfield=pfss_surffield_restore(pfss_time2file('2003-04-05',/surffield))   ;  for users at LMSAL

;  create a magnetogram
nlat0=192  ;  number of latitudinal gridpoints in magnetogram
pfss_mag_create,mag,0,nlat0,file=sfield;,/quiet

;  next get PFSS coefficients
rss=2.0  ;  source surface radius
pfss_get_potl_coeffs,mag,rtop=rss

;  next reconstruct the coronal field in a spherical shell between 1 and rss
pfss_potl_field,rss,2,/trunc;,/quiet

;  record some diagnostic info from this extrapolation
cth=cos(theta)
monopole=(mean_dtheta(total((br(*,*,0)),1),cth)/nlon)(0)
surfflux=mean_dtheta(total((abs(br(*,*,0))),1),cth)*nlat*1e18*rix(0)^2
openflux=mean_dtheta(total((abs(br(*,*,nr-1))),1),cth)*nlat*1e18*rix(nr-1)^2
monfrac=monopole*nlon*nlat*1e18/[surfflux,openflux]
r2inv=rebin(reform(1./rix^2,1,1,nr),nlon,nlat,nr)
br=br-float(monopole*r2inv)  ;  removes monopole from coronal field

;  print some of the diagnostic info
print,'monopole = ',monopole
print,'unsigned flux = ',surfflux
print,'open flux =     ',openflux
print,'monopole fractions = ',monfrac

;  at this point field lines can be drawn, etc., as in pfss_sample1.pro

end
