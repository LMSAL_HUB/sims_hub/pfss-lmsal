;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  scim.pro - (Re)scales input image for display
;
;  usage:
;  scim,im,mag=mag,win=win,scale=scale,top=top,bot=bot,true=true,$
;            outim=outim,/interp,/nowin,/pixmap,/quiet,$
;            ortho=ortho,olon=olon,olat=olat,/white
;            
;          where im = image to be scaled
;                mag = magnification factor (default=1)
;                win = window number to open (default=0)
;                scale = image scaling: if not set, then image scale is
;                        [min(im),max(im)], unless image is a byte image in 
;                        which case the scaling is set to [0,255]; to 
;                        override, set to a scalar for an image scale of 
;                        [-scale,scale], or to a vector for an image scale 
;                        of [scale(0),scale(1)]
;                top = maximum value of scaled result (default=255)
;                bot = minimum value of scaled result (default=0)
;                true = if true color, set to 1,2 or 3 to indicate plane
;                outim = variable into which to put output image
;                interp = for enlargement, use interpolation rather than
;                         pixel replication if magnification factor is
;                         integral (for non-integral magnification
;                         factors, interpolation is always used)
;                nowin = if set, no window is opened (usually used in
;                        conjunction with outim keyword)
;                pixmap = if set, open a window, but do a pixmap
;                quiet = if set, output printed to screen is suppressed
;                ortho = set to [lcent,bcent] if orthographic projection is 
;                        desired, where lcent and bcent are the longitude and 
;                        latitude (respectively) of the tangent point of the 
;                        projection, default=[0,35]
;                olon = vector of longitudes, for orthographic projection
;                olat = vector of latitudes, for orthographic projection
;                white = if set, for orthographic projection, pixels that 
;                        fall out of the map area are write rather than black
;
;  notes: - for orthographic projections, image should be dimensioned [lon,lat]
;           and if nowin desired, set z-buffer and tvrd() to get outim
;         - also for ortho projections, the white keyword will always set the 
;           background to 255b, regardless of what the top keyword is set to
;
;  M.DeRosa -  4 Nov 1998 - added this intro
;              4 Nov 1998 - added capability for non-integral magnifications
;             20 Aug 1999 - nowin keyword supersedes opw and pixmap keywords
;             24 Aug 1999 - changed default enlargement to pixel
;                           replication rather than interpolation for
;                           integral magnification factors
;             11 Dec 1999 - added quiet keyword
;              6 Jun 2000 - added on_error statement
;             24 Jan 2001 - added back pixmap keyword
;              5 Oct 2001 - added ortho,olon,olat keywords
;             18 Mar 2002 - now reforms image at beginning
;             29 Mar 2002 - if a byte image, now scales to [0,255]
;             22 May 2002 - does not open a new window for non-orthogonal
;                           projections if current window already matches 
;                           desired size of image
;             30 Jul 2002 - added white keyword
;             24 Oct 2002 - can now accomodate z-buffer as well
;             17 Feb 2003 - map_set requires a longitude between 0 and 360,
;                           so routine now takes lcent mod 360
;             21 Feb 2003 - now ensures that ortho image is expected size
;              9 Jan 2006 - now deals with true colors via true keyword
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

pro scim,im,mag=mag,win=win,interp=interp,scale=scale,top=top,bot=bot,$
  true=true,outim=outim,nowin=nowin,pixmap=pixmap,quiet=quiet,ortho=ortho,$
  olon=olon,olat=olat,white=white

;  error control
on_error,2

;  usage message
if n_params() eq 0 then begin
  print,'  scim,im,mag=mag,win=win,scale=scale,top=top,bot=bot,outim=outim,'+$
    'true=true,/interp,/nowin,/pixmap,/quiet,ortho=[lcent,bcent],'+$
    'olon=olon,olat=olat,/white'
  return
endif

;  get image parameters
if n_elements(true) eq 0 then true=0
case true(0) of
  0: true=0
  1: true=1
  2: true=2
  3: true=3
  else: true=0
endcase
im=reform(im,/overwrite)
szim=size(im,/dim)
if true gt 0 then begin
  if n_elements(szim) ne 3 then begin
    print,'  scim.pro:  true color images must have exactly three dimensions'
    return
  endif
  case true of
    1: nax=szim([1,2])
    2: nax=szim([0,2])
    3: nax=szim([0,1])
  endcase
endif else begin
  if n_elements(szim) ne 2 then begin
    print,'  scim.pro:  input image must have exactly two dimensions'
    return
  endif
  nax=szim
endelse
byt=size(im,/type) eq 1
if byt then begin
  immin=0 
  immax=255
endif else immin=min(im,max=immax)

;  check keywords and set parameters
if keyword_set(ortho) then begin
  if n_elements(ortho) lt 2 then begin
    lcent=0.0  &  bcent=35.0  ;  defaults
  endif else begin
    lcent=(ortho(0)+360)mod 360  &  bcent=ortho(1)
  endelse
  ortho=1
  nlon=nax(0)
  if nlon ne n_elements(olon) then lon=(linrange(nlon+1,0,360))(0:nlon-1) $
    else lon=olon
  nlat=nax(1)
  if nlat ne n_elements(olat) then begin
    print,'  scim.pro:  olat does not match input image'
    return
  endif else lat=olat
endif else begin
  ortho=0
  if keyword_set(interp) then pixrep=0 else pixrep=1
endelse
if keyword_set(nowin) then nowin=1 else nowin=0
if keyword_set(pixmap) then pixmap=1 else pixmap=0
if n_elements(mag) eq 0 then mag=1.0 else mag=float(mag)
if n_elements(win) eq 0 then win=0 else win=fix(win)
if n_elements(top) eq 0 then top=255b else top=byte(top(0))
if n_elements(bot) eq 0 then bot=0b else bot=byte(bot(0))
case n_elements(scale) of
  0:  begin  ;  scale keyword is not set
    mn=immin
    mx=immax
    end
  1:  begin  ;  scale keyword is set to a scalar
    mn=-scale(0)
    mx=scale(0)
    end
  else:  begin  ;  scale keyword is set to a vector
    mn=scale(0)
    mx=scale(1)
    end
endcase

;  output some info
if not keyword_set(quiet) then begin
  print,'  scim:  original image min/max is '+strcompress(immin,/r)+'/'+$
    strcompress(immax,/r)
  print,'  scim:  display scale is '+strcompress(mn,/r)+' to '+$
    strcompress(mx,/r)
endif

;  now create outim
naxm=nax*mag
if ortho then begin
  if not nowin then begin
    if !d.name eq 'Z' then begin  ;  note square image size
      device,set_resolution=[round(naxm(1)),round(naxm(1))]
      if keyword_set(white) then erase,255b else erase,0b
    endif else begin  ;  assumes 'x' is the device
      window,win,xs=round(naxm(1)),ys=round(naxm(1)),pixmap=pixmap
    endelse
  endif
  map_set,bcent,lcent,/ortho,/iso,/noer,/nobor,pos=[0,0,1,1]
  if keyword_set(white) then backval=(max(im)>mx)*2 else backval=min(im)<mn
  case true of
    1: begin
      remap=map_image(reform(im(0,*,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap2=map_image(reform(im(1,*,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap3=map_image(reform(im(2,*,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      naxremap=size(remap,/dim)
      remap=transpose([[[remap]],[[remap2]],[[remap3]]],[2,0,1])
      end
    2: begin
      remap=map_image(reform(im(*,0,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap2=map_image(reform(im(*,1,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap3=map_image(reform(im(*,2,*)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap=transpose([[[remap]],[[remap2]],[[remap3]]],[0,2,1])
      end
    3: begin
      remap=map_image(reform(im(*,*,0)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap2=map_image(reform(im(*,*,1)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap3=map_image(reform(im(*,*,2)),x0,y0,xsiz,ysiz,missing=backval,$
        /bilinear,/compress,latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),$
        lonmax=lon(nlon-1))
      remap=[[[remap]],[[remap2]],[[remap3]]]
      end
    else: begin
      remap=map_image(im,x0,y0,xsiz,ysiz,missing=backval,/bilinear,/compress,$
        latmin=lat(0),latmax=lat(nlat-1),lonmin=lon(0),lonmax=lon(nlon-1))
    end
  endcase
  outim=bytscl(remap,min=mn,max=mx,top=top-bot)+bot
  wh=where(remap eq backval,nwh)
  if nwh gt 0 then begin
    if keyword_set(white) then outim(wh)=255b else outim(wh)=0b
  endif
  naxo=size(outim,/dim)
  case true of  ;  compare latitude dimension only (use naxm(1) throughout)
    1: begin
      if naxo(1) ne round(naxm(1)) then outim=outim(*,0:round(naxm(1)-1),*)
      if naxo(2) ne round(naxm(1)) then outim=outim(*,*,0:round(naxm(1)-1))
      end
    2: begin
      if naxo(0) ne round(naxm(1)) then outim=outim(0:round(naxm(1)-1),*,*)
      if naxo(2) ne round(naxm(1)) then outim=outim(*,*,0:round(naxm(1)-1))
      end
    3: begin
      if naxo(0) ne round(naxm(1)) then outim=outim(0:round(naxm(1)-1),*,*)
      if naxo(1) ne round(naxm(1)) then outim=outim(*,0:round(naxm(1)-1),*)
      end
    else: begin
      if naxo(0) ne round(naxm(1)) then outim=outim(0:round(naxm(1)-1),*)
      if naxo(1) ne round(naxm(1)) then outim=outim(*,0:round(naxm(1)-1))
    end
  endcase
  if not nowin then tv,outim,x0,y0,xsiz=xsiz,ysiz=ysiz,true=true
endif else begin
  if true gt 0 then begin
    if mag mod 1 ne 0.0 then begin
      case true of
        1: outim=congrid(im,3,naxm(0),naxm(1),/interp)
        2: outim=congrid(im,naxm(0),3,naxm(1),/interp)
        3: outim=congrid(im,naxm(0),naxm(1),3,/interp)
        else: begin
          print,'  scim.pro: logic error with true colors'
          return
          end
      endcase
    endif else begin
      case true of
        1: outim=rebin(im,3,nax(0)*mag,nax(1)*mag,sample=pixrep)
        2: outim=rebin(im,nax(0)*mag,3,nax(1)*mag,sample=pixrep)
        3: outim=rebin(im,nax(0)*mag,nax(1)*mag,3,sample=pixrep)
        else: begin
          print,'  scim.pro: logic error with true colors'
          return
          end
      endcase
    endelse
    outim=bytscl(outim,min=mn,max=mx,top=top-bot)+bot
    if not nowin then begin
      if !d.name eq 'Z' then begin
        device,set_resolution=[round(naxm(0)),round(naxm(1))]
      endif else begin  ;  assumes 'x' is the device
        if (!d.window ne win) or (!d.x_size ne round(naxm(0))) or $
          (!d.y_size ne round(naxm(1))) then $
          window,win,xs=round(naxm(0)),ys=round(naxm(1)),pixmap=pixmap
      endelse
      tv,outim,true=true
    endif
  endif else begin
    if mag mod 1 ne 0.0 then outim=congrid(im,naxm(0),naxm(1),/interp) $
      else outim=rebin(im,nax(0)*mag,nax(1)*mag,sample=pixrep)
    outim=bytscl(outim,min=mn,max=mx,top=top-bot)+bot
    if not nowin then begin
      if !d.name eq 'Z' then begin
        device,set_resolution=[round(naxm(0)),round(naxm(1))]
      endif else begin  ;  assumes 'x' is the device
        if (!d.window ne win) or (!d.x_size ne round(naxm(0))) or $
          (!d.y_size ne round(naxm(1))) then $
          window,win,xs=round(naxm(0)),ys=round(naxm(1)),pixmap=pixmap
      endelse
      tv,outim
    endif
  endelse
endelse

end

