;+
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;
;  gaussquad_legendre.pro - This procedure computes the Legendre collocation 
;                           points and integration weights over x in (-1,1)
;
;  usage:  gaussquad_legendre,n,x,w
;          where n = number of gridpoints
;                x = n-element array of collocation points
;                w = n-element array of integration weights
;
;  notes:  -based on Numerical Recipes routine gauleg, section 4.5, p.145
;          -maximum number of iterations is about n for eps=1e-6
;          -probably need higher precision variables for n>500 or so
;          -total(w) should equal 2
;
;  references: -Press, W.H., Flannery, B.P., Teukolsky, S.A., Vitterling, W.T.
;               1992, Numerical Recipes: The Art of Scientific Computing
;               (Cambridge: Cambridge University Press)
;              -Arfken, G. 1985, Mathematical Methods for Physicists (San
;               Diego, Academic Press) 
;
;  M.DeRosa - 2 Oct 2001 - created
;
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;-

pro gaussquad_legendre,n,x,w

;  usage message
if n_params() eq 0 then begin
  print,'  gaussquad_legendre,n,x,w'
  return
endif

;  make sure n is legit
order=long(n(0))
if order lt 2 then begin
  print,'  gaussquad_legendre: n must be at least 2'
  return
endif

;  setup
x=dblarr(order,/noz)
pprime=dblarr(order,/noz)

;  set tolerance
eps=1d-6  ;  probably adequate for real*8 double precision

;  loop through points
for i=1l,(order+1)/2 do begin  ;  symmetric domain, only do half of the points

  ;  starting guess for ith root
  guess=cos(!dpi*(i-0.25d)/(order+0.5d))

  ;  iterate until zero is found
  repeat begin

    ;  starting values for P_(n-1) and P_n, evaluated at guess point
    pnm1=1d0
    pn  =guess

    ;  find P_n evaluated at guess point, Arfken (12.17a) after n+1 replaces n
    for j=2l,order do begin
      pnm2=pnm1
      pnm1=pn
      pn=(guess*(2*j-1)*pnm1-(j-1)*pnm2)/j
    endfor

    ;  compute d P_n / dx evaluated at guess point, basically Arfken (12.26)
    dpndx=order*(guess*pn-pnm1)/(guess*guess-1)

    ;  use Newton's method to improve guess
    oldguess=guess
    guess=oldguess-pn/dpndx

  ;  check tolerance, repeat if answer isn't good enough
  endrep until abs(guess-oldguess) le eps

  ;  fill x and pprime arrays
  x(i-1)=-guess
  x(order-i)=guess
  pprime(i-1)=dpndx  ;  may be off by a minus sign, but it gets squared below
  pprime(order-i)=dpndx  ;  ditto above

endfor

;  calculate weights
w=2/((1-x*x)*pprime*pprime)

end
