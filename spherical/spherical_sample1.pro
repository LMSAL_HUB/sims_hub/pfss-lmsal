;  sample script for running tools in spherical directory

;  to use, do a  .r spherical_sample1  (this file) at the IDL> prompt

;  M.DeRosa - 19 Jan 2006



;  first load a sample dataset, use a PFSS extrapolation
print,'.....getting a sample vector field, storing in structure called sph_data'
pfss_restore,pfss_time2file('2003-04-05',/ssw_cat,/url)  ;  for all users
;pfss_restore,pfss_time2file('2003-04-05')   ;  for users at LMSAL

;  store in spherical_field_data structure
pfss_to_spherical,sph_data

;  now trace some fieldlines through the data
print,'.....now tracing fieldlines through the data'
spherical_field_start_coord,sph_data,5,10,radstart=1.5
spherical_trace_field,sph_data

;  one can either use spherical_draw_field routine to get a single image...
print,'.....rendering the field, storing as an image (called outim), and displaying'
spherical_draw_field,sph_data,outim=outim,bcent=30,lcent=0,imsc=100,xsize=512,ysize=512
window,xsize=512,ysize=512
tv,outim,/true

;  ...or use spherical_trackball_widget to view the fieldlines interactively
print,'.....press any key to launch interactive viewer'
repeat char=get_kbrd(1) until (strlen(char) gt 0)
print
print,'.....click and drag left mouse button to rotate'
print,'.....click and drag right mouse button to zoom'
spherical_trackball_widget,sph_data,imsc=100

end
