;+
; NAME: spherical_image_create
;
; PURPOSE: 
;   This function creates a structure of type spherical_image_data, containing
;   data spanning a spherical surface and its associated index arrays.
;
; CALLING SEQUENCE:
;   result=spherical_image_create(data,lon,lat,radius,palette=palette)
;
; INPUTS:
;   data = a two-dimensional image gridded in longitude and latitude
;   lon = index array defining the longitudes of the image [in degrees]
;   lat = index array defining the latitudes of the image [in degrees]
;   radius = (optional) radius of the spherical surface (default=1)
;   palette = (optional) an instance of IDLgrPalette object class that 
;             specifies the RGB values of the color lookup table associated 
;             with the image
;
; OUTPUTS:
;   result = a structure of type spherical_image_data with all fields defined
;
; NOTES:
;
; MODIFICATION HISTORY:
;   M.DeRosa - 23 Aug 2006 - created
;              27 Jun 2007 - added optional radius argument
;              13 Jul 2007 - added optional palette keyword
;
;-

function spherical_image_create,data,lon,lat,radius,palette=palette

;  usage message
if n_params() lt 3 then begin
  print,' result=spherical_image_create(data,lon,lat,radius,palette=palette)'
  return,-1
endif

;  error checking
nax=size(data,/dim)
if n_elements(nax) ne 2 then begin
  print,'  ERROR in spherical_image_create: data argument must have 2 dimensions'
  return,-1
endif
if n_elements(lon) ne nax(0) then begin
  print,'  ERROR in spherical_image_create: length of lon array does not match data'
  return,-1
endif
if n_elements(lat) ne nax(1) then begin
  print,'  ERROR in spherical_image_create: length of lat array does not match data'
  return,-1
endif

;  create structure
im_data={spherical_image_data}
im_data.image=ptr_new(data)
im_data.lon=ptr_new(lon)
im_data.lat=ptr_new(lat)
im_data.nlon=n_elements(lon)
im_data.nlat=n_elements(lat)
im_data.theta=ptr_new((90-lat)*!dpi/180)
im_data.phi=ptr_new(lon*!dpi/180)
if n_elements(radius) gt 0 then im_data.rad=double(radius(0)) $
  else im_data.rad=1d
if obj_valid(palette) then im_data.palette=palette

return,im_data

end
