;+
;  NAME: spherical_to_pfss
; 
;  PURPOSE:
;    This procedure populates the pfss common block variables with the data
;    stored in the given spherical_field_data structure (see
;    spherical_field_data__define.pro).
; 
;  CALLING SEQUENCE:
;    spherical_to_pfss,sph_data,/noreset,/no_copy
; 
;  INPUTS:
;    sph_data = a structure of type spherical_field_data (see
;      spherical_field_data__define.pro)
;    noreset = if set, routine does not set to undefined variables which are
;              not defined in the spherical field data structure or which
;              exist in the pfss common block but not in the spherical data
;              structure 
;    no_copy = if set, will free the pointers in the sph_data structure after
;              the data is copied to the variables in the pfss common block
; 
;  OUTPUTS: none
; 
;  COMMON BLOCKS:
;    uses pfss common block (see pfss_data_block.pro) as input data
; 
;  NOTES:
;    1.  Those variables in the spherical_field_data structure for which there
;        are no corresponding variables in the pfss common block are not
;        carried over.
;    2.  The temporary function is used here to set variables to undefined.
;    3.  If variables are added to the pfss common block, one needs to add a
;        line to this routine.
;
;  MODIFICATION HISTORY:
;    M.DeRosa - 15 Dec 2005 - created
;               19 Aug 2006 - added noreset keyword
;               13 Apr 2007 - added no_copy keyword
;                3 Aug 2011 - added check to see if sph_data is a structure
; 
;-

pro spherical_to_pfss,sph_data,noreset=noreset,no_copy=no_copy

;  preliminaries
if keyword_set(noreset) then nre=1 else nre=0
if keyword_set(no_copy) then nc=1 else nc=0
if size(sph_data,/type) ne 8 then begin
  print,'  ERROR in spherical_to_pfss: no input structure provided'
  return
endif

;  access pfss common block
@pfss_data_block

if ptr_valid(sph_data.br) then br=*sph_data.br else $
  if ((n_elements(br) gt 0) and (nre eq 0)) then dummy=temporary(br)
if ptr_valid(sph_data.bth) then bth=*sph_data.bth else $
  if ((n_elements(bth) gt 0) and (nre eq 0)) then dummy=temporary(bth)
if ptr_valid(sph_data.bph) then bph=*sph_data.bph else $
  if ((n_elements(bph) gt 0) and (nre eq 0)) then dummy=temporary(bph)
if n_elements(sph_data.nr) gt 0 then nr=sph_data.nr else $
  if ((n_elements(nr) gt 0) and (nre eq 0)) then dummy=temporary(nr)
if n_elements(sph_data.nlat) gt 0 then nlat=sph_data.nlat else $
  if ((n_elements(nlat) gt 0) and (nre eq 0)) then dummy=temporary(nlat)
if n_elements(sph_data.nlon) gt 0 then nlon=sph_data.nlon else $
  if ((n_elements(nlon) gt 0) and (nre eq 0)) then dummy=temporary(nlon)
if ptr_valid(sph_data.rix) then rix=*sph_data.rix else $
  if ((n_elements(rix) gt 0) and (nre eq 0)) then dummy=temporary(rix)
if ptr_valid(sph_data.lat) then lat=*sph_data.lat else $
  if ((n_elements(lat) gt 0) and (nre eq 0)) then dummy=temporary(lat)
if ptr_valid(sph_data.lon) then lon=*sph_data.lon else $
  if ((n_elements(lon) gt 0) and (nre eq 0)) then dummy=temporary(lon)
if ptr_valid(sph_data.theta) then theta=*sph_data.theta else $
  if ((n_elements(theta) gt 0) and (nre eq 0)) then dummy=temporary(theta)
if ptr_valid(sph_data.phi) then phi=*sph_data.phi else $
  if ((n_elements(phi) gt 0) and (nre eq 0)) then dummy=temporary(phi)
if ptr_valid(sph_data.str) then str=*sph_data.str else $
  if ((n_elements(str) gt 0) and (nre eq 0)) then dummy=temporary(str)
if ptr_valid(sph_data.stth) then stth=*sph_data.stth else $
  if ((n_elements(stth) gt 0) and (nre eq 0)) then dummy=temporary(stth)
if ptr_valid(sph_data.stph) then stph=*sph_data.stph else $
  if ((n_elements(stph) gt 0) and (nre eq 0)) then dummy=temporary(stph)
if ptr_valid(sph_data.ptr) then ptr=*sph_data.ptr else $
  if ((n_elements(ptr) gt 0) and (nre eq 0)) then dummy=temporary(ptr)
if ptr_valid(sph_data.ptth) then ptth=*sph_data.ptth else $
  if ((n_elements(ptth) gt 0) and (nre eq 0)) then dummy=temporary(ptth)
if ptr_valid(sph_data.ptph) then ptph=*sph_data.ptph else $
  if ((n_elements(ptph) gt 0) and (nre eq 0)) then dummy=temporary(ptph)
if ptr_valid(sph_data.nstep) then nstep=*sph_data.nstep else $
  if ((n_elements(nstep) gt 0) and (nre eq 0)) then dummy=temporary(nstep)

;  variables in the common block that have no corresponding field in sph_data
if nre eq 0 then begin
  if n_elements(l0) gt 0 then dummy=temporary(l0)
  if n_elements(b0) gt 0 then dummy=temporary(b0)
  if n_elements(now) gt 0 then dummy=temporary(now)
  if n_elements(phiat) gt 0 then dummy=temporary(phiat)
  if n_elements(phibt) gt 0 then dummy=temporary(phibt)
  if n_elements(rimage) gt 0 then dummy=temporary(rimage)
endif

;  free some memory
if nc eq 1 then begin
  if ptr_valid(sph_data.br) then ptr_free,sph_data.br
  if ptr_valid(sph_data.bth) then ptr_free,sph_data.bth
  if ptr_valid(sph_data.bph) then ptr_free,sph_data.bph
  if ptr_valid(sph_data.rix) then ptr_free,sph_data.rix
  if ptr_valid(sph_data.lat) then ptr_free,sph_data.lat
  if ptr_valid(sph_data.lon) then ptr_free,sph_data.lon
  if ptr_valid(sph_data.theta) then ptr_free,sph_data.theta
  if ptr_valid(sph_data.phi) then ptr_free,sph_data.phi
  if ptr_valid(sph_data.str) then ptr_free,sph_data.str
  if ptr_valid(sph_data.stth) then ptr_free,sph_data.stth
  if ptr_valid(sph_data.stph) then ptr_free,sph_data.stph
  if ptr_valid(sph_data.ptr) then ptr_free,sph_data.ptr
  if ptr_valid(sph_data.ptth) then ptr_free,sph_data.ptth
  if ptr_valid(sph_data.ptph) then ptr_free,sph_data.ptph
  if ptr_valid(sph_data.nstep) then ptr_free,sph_data.nstep
  sph_data=-1
endif

end
