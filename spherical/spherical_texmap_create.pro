;+
; NAME: spherical_texmap_create
;
; PURPOSE: 
;   This function creates an object of spherically gridded data mapped onto a
;   spherical surface.  The output is an object of class IDLgrPolygon.
;
; CALLING SEQUENCE:
;   result=spherical_texmap_create(sphim_data,radius,imsc=imsc,lonbounds=lonbounds)
;
; INPUTS:
;   sphim_data=a structure of type spherical_image_data
;   radius=radius of spherical surface onto which the image is to be mapped
;   imsc=data value(s) to which to scale central magnetogram image; if only
;        one value is given then range is [-value,+value] (default = [-image
;        max,+image max], centered around 0)
;   lonbounds=array of bounds [lonmin,lonmax], in degrees between 0 and 360,
;             defining data that is bounded in longitude (otherwise, the data
;             is assumed to wrap around), and note that order matters!  If
;             lonbounds(0) is <0 or undefined, then it is assumed that the
;             data span all longidudes
;
; OUTPUTS:
;   result=an object of class IDLgrPolygon containing the texture map
;
; NOTES: 
;   -Assumes phi data is monotonically increasing
;
; MODIFICATION HISTORY:
;   M.DeRosa - 23 Aug 2006 - created
;
;-

function spherical_texmap_create,sphim_data,radius,imsc=imsc,$
  lonbounds=lonbounds

;  usage message
if n_params() eq 0 then begin
  print,'  result=spherical_texmap_create(sphim_data,radius,imsc=imsc,'+ $
    'lonbounds=lonbounds)'
  return,-1
endif

;  deal with phi bounds
if n_elements(lonbounds) gt 0 then begin
  if (lonbounds(0) ge 0) and (lonbounds(1) ne 0) then begin
    ph1=lonbounds(0)*!dpi/180
    ph2=lonbounds(1)*!dpi/180
    bounded=1b
  endif else bounded=0b
endif else bounded=0b

;  deal with theta bounds
thmin=min(*sphim_data.theta,max=thmax)

;  if global data, then replicate last longitude so that no seam in the
;  texture map appears
imd2=sphim_data
if bounded eq 0 then begin
  imd2.image=ptr_new([*sphim_data.image,(*sphim_data.image)(0,*)])
  imd2.lon=ptr_new([*sphim_data.lon,360+(*sphim_data.lon)(0)])
  imd2.phi=ptr_new([*sphim_data.phi,2*!dpi+(*sphim_data.phi)(0)])
  imd2.nlon=sphim_data.nlon+1
endif

;  get byte image and create texture map object
if not keyword_set(imsc) then imsc=max(abs(minmax(*sphim_data.image)))
scim,*imd2.image,outim=byteimage,sc=imsc,/nowin,/quiet,top=249
otexmap=obj_new('IDLgrImage',data=byteimage)

;  create vertex list for polygonal spherical surface at the lower radius
if not keyword_set(radius) then radius=1.0
phgrid=(*imd2.phi)#replicate(1,imd2.nlat)
thgrid=!dpi/2-(replicate(1,imd2.nlon)#(*imd2.theta))
radgrid=replicate(radius,imd2.nlon,imd2.nlat)
sph_vert=transpose([[[phgrid]],[[thgrid]],[[radgrid]]],[2,0,1])
sph_vert=reform(sph_vert,3,imd2.nlon*imd2.nlat,/overwrite)
rect_vert=cv_coord(from_sph=sph_vert,/to_rect)

;  create the connectivity array
connect=lonarr(5,imd2.nlon-1,imd2.nlat-1)
for i=0,(imd2.nlat-1)-1 do for k=0,(imd2.nlon-1)-1 do begin
  stpt=i*imd2.nlon+k  ;  index of starting point in vertex list
  connect(*,k,i)=[4,stpt,stpt+1,stpt+1+imd2.nlon,stpt+imd2.nlon]
endfor

;  determine the texture map coordinates
if bounded then begin
  texture_coord=[(sph_vert(0,*)-ph1)/(ph2-ph1),$
    (sph_vert(1,*)-(!dpi/2-thmin))/(thmax-thmin)]
endif else begin
  texture_coord=[sph_vert(0,*)/(2*!dpi),$
    (sph_vert(1,*)-(!dpi/2-thmin))/(thmax-thmin)]
endelse

;  add the texture mapped image to the model
osphere=obj_new('IDLgrPolygon',data=rect_vert,polygons=connect,col=[255,255,255],$
  style=2,texture_map=otexmap,texture_coord=texture_coord,/texture_interp)

;  return
return,osphere

end
