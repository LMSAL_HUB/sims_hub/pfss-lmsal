;+
; NAME: spherical_get_radial_crossing
;
; PURPOSE:
;   Given vector fieldline data on a spherical grid, this function
;   determines the latitudes and longitudes at which each fieldline
;   intersects a surface of constant radius.
;
; CALLING SEQUENCE:
;   result=spherical_get_radial_crossing(sph_data,rcut)
;
; INPUTS:
;   sph_data = a structure of type spherical_field_data (see
;      spherical_field_data__define.pro) with the following fields defined on
;      input: br,bth,bph,nlat,nlon,nr,rix,thix,phix,lat,lon,str,stth,stph.
;      Basically, one needs the vector field (br,bth,bph), its dimension
;      (nr,nlat,nlon), its indexing (rix,thix,phix,lat,lon), and the starting
;      points (str,stth,stph).
;   rcut = radius of spherical surface at which to determine crossings
;
; OUTPUTS:
;   result = [3,n] array of data where each of the n crossings has a
;      row in the array: [lineno,bcross,lcross] where lineno = the
;      line number of the input fieldline array in the sph_data
;      structure, and (bcross,lcross) are the colatitude and longitude
;      in radians of the crossing.  If no crossings are detected, or
;      if there is an error, the result will be -1.
;   interpindex = on output, an n-element array of interpolation
;      coordinates giving the fractional gridpoint along each
;      fieldline at which the nth crossing occurs
;
; MODIFICATION HISTORY:
;   M.DeRosa - 23 Mar 2007 - created, based on pfss_rad_field_crossing.pro
;               7 Jan 2010 - added interpindex keyword
;
;-

function spherical_get_radial_crossing,sph_data,rcut,interpindex=interpindex

;  usage message
if n_elements(sph_data) eq 0 then begin
  print,'  ERROR in spherical_get_radial_crossing: no input data provided'
  print,'  Calling sequence: result=spherical_get_radial_crossing(sph_data,rcut)'
  return,-1
endif

;  check rcut
if n_elements(rcut) eq 0 then begin
  print,'  ERROR in spherical_get_radial_crossing: rcut is not specified'
  return,-1
endif else rcut=rcut(0)
rmin=min(*sph_data.rix,max=rmax)
if (rcut lt rmin) or (rcut gt rmax) then begin
  print,'  WARNING in spherical_get_radial_crossing: rcut out of bounds,'
  print,'    using closest radial gridpoint'
  rcut=rmin>rcut<rmax
endif

;  initialize output array
result=[-1.,-1.,-1.]
interpindex=[-1.0]

;  loop through the individual field lines
nlines=n_elements(*sph_data.nstep)
for i=0l,nlines-1 do begin

  ;  extract coordinates of current field line
  ns=(*sph_data.nstep)(i)
  lr=(*sph_data.ptr)(0:ns-1,i)
  lth=(*sph_data.ptth)(0:ns-1,i)
  lph=(*sph_data.ptph)(0:ns-1,i)
  
  ;  look for crossings only if rcut is in range
  lmin=min(lr,max=lmax)
  if (rcut le lmax) and (rcut ge lmin) then begin

    ;  first find out if any points exactly match, and set them
    ixm=where(lr eq rcut,nmatch)
    if nmatch gt 0 then begin
      for j=0,nmatch-1 do begin
        result=[[result],[i,lth(ixm(j)),lph(ixm(j))]]
        interpindex=[interpindex,ixm(j)]
      endfor
    endif

    ;  now find crossings
    ixpt=where(((lr(0:ns-2)-rcut)*(lr(1:ns-1)-rcut)) lt 0,ncr)
    if ncr gt 0 then begin

      ;  linearly interpolate to get locus of crossing
      for j=0l,ncr-1 do begin
        if total(ixm eq ixpt(j)) eq 0 then begin  ;  avoids double-counting
          coeff=(rcut-lr(ixpt(j)))/(lr(ixpt(j)+1)-lr(ixpt(j)))
          ptcrth=(1-coeff)*lth(ixpt(j))+coeff*lth(ixpt(j)+1)
          ptcrph=(((1-coeff)*lph(ixpt(j))+coeff*lph(ixpt(j)+1))+(2*!dpi)) $
            mod (2*!dpi)
          result=[[result],[i,ptcrth,ptcrph]]  
          interpindex=[interpindex,ixpt(j)+coeff]    
        endif
      endfor

    endif

  endif

endfor

;  remove first row of output array
if n_elements(result) eq 3 then begin
  print,'  WARNING in spherical_get_radial_crossing:  no crossings detected'
  result=-1
endif else begin
  result=result(*,1:*)
  interpindex=interpindex(1:*)
endelse

return,result

end
